let users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];

console.log("Original Array:");
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

const functionAdd = function (input) {
  users.push(input);
  console.log(users);
};

functionAdd("John Cena");

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

const functionIndex = function (index) {
  const found = users[index];
  return found;
};
const itemFound = functionIndex(2);
console.log(itemFound);

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

const functionPop = function () {
  const lastItem = users.pop();
  console.log(lastItem);
  return lastItem;
};
functionPop();
console.log(users);

/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

const functionUpdateItem = function (update, indexNum) {
  users[indexNum] = update;
};
functionUpdateItem("Triple H", 3);
console.log(users);

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

const functionDeleteAll = function () {
  users.splice(0, users.length);
};
functionDeleteAll();
console.log(users);

/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

const functionCheckEmpty = function (arr) {
  if (arr.length > 0) {
    return false;
  } else {
    return true;
  }
};

const isUsersEmpty = functionCheckEmpty(users);
console.log(isUsersEmpty);

// let grades = [98.5, 94.3, 89.2, 90.1];
// let computer_brands = ['Acer', 'Asus', 'Lenovo', 'Fujitsu', 'MSI', 'Razer'];

// // You can mix data type within the same array, however it is not recommended to do so.
// let mixed_arr = [12, 'Asus', null, undefined, {}];

// // Re-assigning values inside an array
// let my_tasks = [
// 	'drink html',
// 	'eat javascript',
// 	'snort css',
// 	'bake sass'
// ];

// // Before reassigning
// console.log(my_tasks);

// // We use the name of the array and put the index of the item we want to access/modify
// my_tasks[0] = 'hello world';

// // After reassigning
// console.log(my_tasks);

// // To get a specific value from an array
// console.log(grades[0]);
// console.log(computer_brands[3]);

// // To get the length of the array
// console.log(computer_brands.length);

// if(computer_brands.length > 5){
// 	console.log('We have too many suppliers my guy.');
// }

// // To get the last item inside an array
// let last_item_index = computer_brands.length - 1

// console.log(computer_brands[last_item_index]);

// // [Section] Array Methods
// let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit", "Passionfruit"];

// console.log("Current status of array:");
// console.log(fruits);

// // The push() method inserts a new value into the end of the array.
// fruits.push('Mango');

// // Note: Everytime an array is modified, it is called 'Mutation'.
// console.log("Mutated array from push method");
// console.log(fruits);

// // For pushing multiple items to an array
// fruits.push('Avocado', 'Pineapple');
// console.log(fruits);

// // The pop() method removes the last item in the array and returns it.
// let removed_fruit = fruits.pop();
// console.log(removed_fruit);
// console.log(fruits);

// // The unshift() method adds one or more items to the beginning of the array
// fruits.unshift('Lime', 'Banana');
// console.log(fruits);

// // The shift() method removes the first item in the array
// fruits.shift();
// console.log(fruits);

// // The splice() method replaces the value of one or two specified items. The first argument (1) serving as the index where the replacement will start and the second argument (2) serving as the number of items to be replaced starting from the value of the first argument.
// fruits.splice(1, 2, "Lime", "Cherry");
// console.log(fruits);

// // The sort() method sorts the items in the array in alphabetical/alphanumeric order
// fruits.sort();
// console.log(fruits);

// // The reverse() method reverses the order of the array items.
// fruits.reverse();
// console.log(fruits);

// // The indexOf() method gets the index of a specific item in the array.
// let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// let first_index = countries.indexOf('SG');
// let invalid_country = countries.indexOf('BR');

// console.log(first_index);
// console.log(invalid_country);

// // The slice method removes a part of the array by specifying the index in which the slice will start/end. That part can then be accessed from the array.slice() method itself.
// // Note: The slice() method will not affect the original array itself.
// let sliced_array_a = countries.slice(2);
// console.log(sliced_array_a);

// let sliced_array_b = countries.slice(2, 4);
// console.log(sliced_array_b);

// // The toString() method converts the data type of the array into a string and adds commas as a separator for each item in the array.
// let string_array = countries.toString();
// console.log(string_array);

// // The join() method joins each item in the array and allows you to customize the separator to be used. If no separator is provided then Javascript will use commas as the default separator.
// let users = ['John', 'Snow', 'Virgo', 'Naruto', 'Earl'];

// console.log(users.join(' - '));
// console.log(users.join());

// // The forEach() method is a way to loop through all the items in an array. It takes an anonymous function (nameless function) as an argument and runs that function everytime the loop iterates.
// users.forEach(function(user) { // The 'user' parameter represents each individual item in the array for each iteration of the loop.
// 	console.log(user)
// })

// // The map() method loops through the array BUT it returns data depending on the operation within its function.
// let numbers = [1, 2, 3, 4, 5];

// let number_map = numbers.map(function(number) {
// 	return number * number;
// })

// console.log(number_map);

// // The filter() method filters the array based on the condition within the anonymous function. It returns a new array with items that have met the condition that was set.
// let filter_valid = numbers.filter(function(number){
// 	return (number < 3);
// })

// console.log(filter_valid);

// // [Section] Multidimensional Arrays
// let chess_board = [
// 	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
// 	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
// 	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
// 	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
// 	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
// 	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
// 	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
// 	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
// ];

// console.log(chess_board);
// console.log(chess_board[2][7]);
